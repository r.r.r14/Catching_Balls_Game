﻿using UnityEngine;

public struct RectData
{
    public float leftBorder { get; private set; }
    public float topBorder { get; private set; }
    public float rightBorder { get; private set; }
    public float botBorder { get; private set; }

    public RectData(float size) : this()
    {
        this.leftBorder = -size;
        this.topBorder = size;
        this.rightBorder = size;
        this.botBorder = -size;
    }

    public RectData(Transform topLeftPoint, Transform botRightPoint) 
           : this()
    {
        this.leftBorder = topLeftPoint.position.x;
        this.topBorder = topLeftPoint.position.y;
        this.rightBorder = botRightPoint.position.x;
        this.botBorder = botRightPoint.position.y;
    }
}

