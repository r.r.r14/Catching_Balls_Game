﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    #region Public Fields

    [Header("Options:")]
    public float indent = 0f;

    [Header("Platforms")]
    [SerializeField] private PlatformBinding[] leftPlatforms;
    [SerializeField] private PlatformBinding[] rightPlatforms;

    [Header("Movement Rect")]
    [SerializeField] private Transform topLeftPoint;
    [SerializeField] private Transform botRightPoint;

    public RectData movementRect { get; private set; }

    public float minScreenPositionX { get; private set; }
    public float maxScreenPositionX { get; private set; }

    #endregion

    #region Private Fields


    #endregion


    void Awake()
    {
        minScreenPositionX =
            GameController.ConvertToWorld(new Vector2(0, 0)).x - indent;
        maxScreenPositionX = -minScreenPositionX;

        // First Set Binding to left and right platforms
        SetPlatformsBinding(minScreenPositionX, leftPlatforms);
        SetPlatformsBinding(maxScreenPositionX, rightPlatforms);

        // Then get Border Point position
        movementRect = new RectData(topLeftPoint, botRightPoint);
    }

    #region Private Methods

    private void SetPlatformsBinding(float xPosition,
                                     PlatformBinding[] platforms)
    {
        // ReSharper disable once ForCanBeConvertedToForeach
        // foreach create an extra copies
        for (int i = 0; i < platforms.Length; i++)
        {
            platforms[i].SetBindingByX(xPosition);
        }
    }

    #endregion
}
