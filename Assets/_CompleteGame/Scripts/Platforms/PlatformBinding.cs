﻿using UnityEngine;

public class PlatformBinding : MonoBehaviour
{
    #region Private Fields

    private Transform _thisTransform;

    #endregion

    void Awake()
    {
        _thisTransform = transform;
    }

    #region Public Methods

    /// <summary>
    /// Change GameObject by X position
    /// </summary>
    /// <param name="xPosition"></param>
    public void SetBindingByX(float xPosition)
    {
        SetBinding(new Vector2(xPosition, _thisTransform.position.y));
    }

    /// <summary>
    /// Change GameObject by Y position
    /// </summary>
    /// <param name="yPosition"></param>
    public void SetBindingByY(float yPosition)
    {
        SetBinding(new Vector2(_thisTransform.position.x, yPosition));
    }

    /// <summary>
    /// Change game object position
    /// </summary>
    /// <param name="position"></param>
    public void SetBinding(Vector2 position)
    {
        _thisTransform.position = position;
    }

    #endregion
}   
