﻿using UnityEngine;
using UnityEngine.XR.WSA.WebCam;

public class GameController : MonoBehaviour {

    #region Public Fields

    public static Camera mainCamera { get; private set; }

    #endregion

    void Awake()
    {
        mainCamera = Camera.main;
    }

    #region Static Features

    /// <summary>
    /// Convert pixel position to world position ( MainCamera )
    /// </summary>
    public static Vector2 ConvertToWorld(Vector2 pixelPosition)
    {
        return mainCamera.ScreenToWorldPoint(pixelPosition);
    }

    #endregion
}
