﻿using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    #region Public Fields

    [SerializeField] private PlatformController platformController;

    #endregion

    #region Private Fields

    private RectData _movementRect;

    private PlayerMotor _motor;

    #endregion

    void Awake()
    {
        _motor = GetComponent<PlayerMotor>();
    }

    void Start()
    {
        if (ReferenceEquals(platformController, null))
        {
            // Set default borders 
            _movementRect = new RectData(float.MaxValue);
            return;
        }
        _movementRect = platformController.movementRect;
    }

	void Update ()
	{
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = GameController.ConvertToWorld(Input.mousePosition);
            float xPos = Mathf.Clamp(mousePosition.x,
                _movementRect.leftBorder, _movementRect.rightBorder);
            float yPos = Mathf.Clamp(mousePosition.y,
                _movementRect.botBorder, _movementRect.topBorder);
            Vector2 movement = new Vector2(xPos, yPos);

            _motor.movement = movement;
        }

     //   if (Input.touchCount > 0)
	    //{
     //       Vector2 mousePosition = _cam.ScreenToWorldPoint(Input.GetTouch(0).position);
	    //    float xPos = Mathf.Clamp(mousePosition.x,
	    //        _movementRect.leftBorder, _movementRect.rightBorder);
	    //    float yPos = Mathf.Clamp(mousePosition.y,
	    //        _movementRect.botBorder, _movementRect.topBorder);
     //       Vector2 movement = new Vector2(xPos, yPos);

	    //    _motor.movement = movement;
	    //}
    }
}
