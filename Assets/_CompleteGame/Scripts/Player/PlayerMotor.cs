﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMotor : MonoBehaviour
{
    #region Public Fields

    #region Movement

    [Header("Movement")]
    public float movementSpeed = 10f;
    public float smoothMoveTime = 0.3f;

    [HideInInspector] public Vector2 movement = Vector2.zero;

    #endregion

    #region Rotation

    [Header("Rotation")]
    public float angleToRotate = 30f;
    public float rotationSpeed = 100f;
    public float smoothRotTime = 0.1f;

    [SerializeField] private Transform pointToAccelerate;

    #endregion

    #endregion

    #region Private Fields

    #region Movement

    private Vector2 _movementVelocity = Vector2.zero;

    #endregion

    #region Rotation

    private float _angleVelocity;

    #endregion

    private Rigidbody2D _body;

    #endregion

    void Awake()
    {       
        _body = this.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate ()
    {
        Move();
        Rotate();
    }

    #region Private Methods

    private void Move()
    {
        // Smooth movement
        Vector2 smoothMovement =
            Vector2.SmoothDamp(_body.position, this.movement,
                               ref _movementVelocity, smoothMoveTime, movementSpeed);

        _body.MovePosition(smoothMovement);
    }

    private void Rotate()
    {
        float targetRotation = _body.position.x > pointToAccelerate.position.x
                               ? -angleToRotate
                               : angleToRotate;

        // Smooth rotation
        float smoothAngle =
            Mathf.SmoothDampAngle(_body.rotation, targetRotation,
                ref _angleVelocity, smoothRotTime, rotationSpeed);

        _body.MoveRotation(smoothAngle);
    }

    #endregion
}
